// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.



function carBefore2000(inventory) {
    let arrOfYear = [];
    for (let index = 0; index < inventory.length; index++) {
        arrOfYear.push(inventory[index].car_year);
    }
        if (arrOfYear.length > 0) {
           return arrOfYear;
        } else {
           return "inventory doesn't have car_year";
        }
}


module.exports = carBefore2000;
